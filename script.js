const btn = document.querySelector(".btn");
btn.addEventListener('click', getLocation);

async function getLocation() {
    const {
        ip
    } = await fetchJSON("https://api.ipify.org/?format=json");
    const location = await fetchJSON(`http://ip-api.com/json/${ip}`)
    
    const info = {
        country: location.country,
        region: location.regionName,
        city: location.city,
        district: location.district
    }

    const infoContainer = document.querySelector(".location-info");
    infoContainer.classList.add('active');

    Object.entries(info).forEach((entry) => {
        const element = document.createElement("div");
        element.innerHTML = `${entry[0]} - ${entry[1]}`;
        infoContainer.appendChild(element);
    })
}

const fetchJSON = (url, initRequest) =>
    fetch(url, initRequest)
    .then(response => response.json());